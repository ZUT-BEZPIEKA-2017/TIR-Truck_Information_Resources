CREATE TABLE IF NOT EXISTS Admin (
  idAdmin INT NOT NULL,
  Imie VARCHAR(45) NOT NULL,
  Nazwisko VARCHAR(45) NOT NULL,
  Email VARCHAR(45) NOT NULL,
  Haslo VARCHAR(64) NOT NULL,
  Login VARCHAR(45) NOT NULL,
  PRIMARY KEY (idAdmin));
  
CREATE TABLE IF NOT EXISTS Kierowca (
  idKierowca INT NOT NULL,
  Imie VARCHAR(45) NOT NULL,
  Nazwisko VARCHAR(45) NOT NULL,
  Email VARCHAR(45) NOT NULL,
  Haslo VARCHAR(64) NOT NULL,
  OstaniaZmianaHasla DATE NOT NULL,
  PRIMARY KEY (idKierowca));

CREATE TABLE IF NOT EXISTS Parking (
  idParking INT NOT NULL,
  Adres VARCHAR(45) NOT NULL,
  Wspolrzedne_Lat DECIMAL(10,7) NOT NULL,
  Wspolrzedne_Lng DECIMAL(10,7) NOT NULL,
  Ilosc_Miejsc INT NOT NULL,
  PRIMARY KEY (idParking));

CREATE TABLE IF NOT EXISTS Parking_Zgloszenia (
  idParking_Miejsca INT NOT NULL,
  Miejsca INT NOT NULL,
  idKierowca INT NOT NULL,
  idParking INT NOT NULL,
  PRIMARY KEY (idParking_Miejsca),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca),
  FOREIGN KEY (idParking) REFERENCES Parking (idParking) );

CREATE TABLE IF NOT EXISTS Rodzaj_Atrakcji (
  idRodzaj_Atrakcji INT NOT NULL,
  Typ VARCHAR(45) NOT NULL,
  PRIMARY KEY (idRodzaj_Atrakcji));

CREATE TABLE IF NOT EXISTS Atrakcje (
  idAtrakcje INT NOT NULL,
  Nazwa VARCHAR(45) NOT NULL,
  Informacje VARCHAR(250) NOT NULL,
  idRodzaj_atrakcji INT NOT NULL,
  Wspolrzedne_Lng DECIMAL(10,7) NOT NULL,
  Wspolrzedne_Lat DECIMAL(10,7) NOT NULL,
  PRIMARY KEY (idAtrakcje),
  FOREIGN KEY (idRodzaj_atrakcji) REFERENCES Rodzaj_Atrakcji (idRodzaj_Atrakcji) );

CREATE TABLE IF NOT EXISTS Kontakty (
  idKontakty INT NOT NULL,
  idKierowca INT NOT NULL,
  idKierowca_Znajomy INT NOT NULL,
  PRIMARY KEY (idKontakty),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca),
  FOREIGN KEY (idKierowca_Znajomy) REFERENCES Kierowca (idKierowca) );

CREATE TABLE IF NOT EXISTS Ulubione_Miejsca (
  idUlubione_Miejsca INT NOT NULL,
  idKierowca INT NOT NULL,
  idAtrakcje INT NOT NULL,
  PRIMARY KEY(idUlubione_Miejsca),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca),
  FOREIGN KEY (idAtrakcje) REFERENCES Atrakcje (idAtrakcje) );

CREATE TABLE IF NOT EXISTS Konwersacje (
  idKonwersacje INT NOT NULL,
  Tresc VARCHAR(200) NOT NULL,
  Czas DATETIME(2) NOT NULL,
  idKierowca_Nadawca INT NOT NULL,
  idKierowca_Odbiorca INT NOT NULL,
  idKontakty INT NOT NULL,
  PRIMARY KEY (idKonwersacje),
  FOREIGN KEY (idKierowca_Nadawca) REFERENCES Kierowca (idKierowca), 
  FOREIGN KEY (idKierowca_Odbiorca) REFERENCES Kierowca (idKierowca),
  FOREIGN KEY (idKontakty) REFERENCES Kontakty (idKontakty) );

CREATE TABLE IF NOT EXISTS Czas_Pracy (
  idCzas_Pracy INT NOT NULL,
  Jazda_Start DATETIME(2) NOT NULL,
  Jazda_Koniec DATETIME(2) NOT NULL,
  Czas TIME(2) NOT NULL,
  idKierowca INT NOT NULL,
  PRIMARY KEY (idCzas_Pracy),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca) );

CREATE TABLE IF NOT EXISTS Raport_Praca (
  idRaport_Praca INT NOT NULL,
  Data DATE NOT NULL,
  idCzas_Pracy INT NOT NULL,
  PRIMARY KEY (idRaport_Praca),
  FOREIGN KEY (idCzas_Pracy) REFERENCES Czas_Pracy (idCzas_Pracy) );

CREATE TABLE IF NOT EXISTS Typ_Przeszkoda (
  idTyp_Przeszkoda INT NOT NULL,
  Nazwa VARCHAR(45) NOT NULL,
  PRIMARY KEY (idTyp_Przeszkoda));

CREATE TABLE IF NOT EXISTS Przeszkody_Zgloszenia (
  idPrzeszkody_Zgloszenia INT NOT NULL,
  idKierowca INT NOT NULL,
  idTyp_Przeszkoda INT NOT NULL,
  Kiedy DATETIME(2) NOT NULL,
  Aktualnosc TINYINT(1) NOT NULL,
  Wspolrzedne_Lng DECIMAL(10,7) NOT NULL,
  Wspolrzedne_Lat DECIMAL(10,7) NOT NULL,
  PRIMARY KEY (idPrzeszkody_Zgloszenia),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca),
  FOREIGN KEY (idTyp_Przeszkoda) REFERENCES Typ_Przeszkoda (idTyp_Przeszkoda) );

CREATE TABLE IF NOT EXISTS Hasla_Historia_Kierowca (
  idHasla_Historia INT NOT NULL,
  Haslo VARCHAR(64) NOT NULL,
  Data DATE NOT NULL,
  idKierowca INT NOT NULL,
  PRIMARY KEY (idHasla_Historia),
  FOREIGN KEY (idKierowca) REFERENCES Kierowca (idKierowca) );

CREATE TABLE IF NOT EXISTS Hasla_Historia_Admin (
  idHasla_Historia_Admin INT NOT NULL,
  Haslo VARCHAR(64) NOT NULL,
  Data DATE NOT NULL,
  idAdmin INT NOT NULL,
  PRIMARY KEY (idHasla_Historia_Admin),
  FOREIGN KEY (idAdmin) REFERENCES Admin (idAdmin) );
