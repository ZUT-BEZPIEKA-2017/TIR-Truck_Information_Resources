﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Parking
    {
        public int idParking { get; set; }
        public string Adres { get; set; }
        public double Wspolrzedne_Lat { get; set; }
        public double Wspolrzedne_Lng { get; set; }
        public int Ilosc_Miejsc { get; set; }
    }
}