﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Kierowca
    {
        public int idKierowca { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public string Haslo { get; set; }
        public string OstatniaZmianaHasla { get; set; }
        public string Login { get; set; }
    }
}