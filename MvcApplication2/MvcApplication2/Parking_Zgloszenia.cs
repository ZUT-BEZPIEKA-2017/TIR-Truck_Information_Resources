﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Parking_Zgloszenia
    {
        public int idParking_Miejsca { get; set; }
        public int Miejsca { get; set; }
        public int idKierowca { get; set; }
        public int idParking { get; set; }
        public String Czas { get; set; }
    }
}