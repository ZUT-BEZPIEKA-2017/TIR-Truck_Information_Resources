﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;

namespace MvcApplication2.Controllers
{
    public class KierowcaController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        [Authorize]
        [RequireHttps]
        public IEnumerable<Kierowca> Get()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            List<Kierowca> retVal = new List<Kierowca>();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Kierowca]");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(new Kierowca
                            {
                                idKierowca = reader.GetInt32(0),
                                Imie = reader.GetString(1),
                                Nazwisko = reader.GetString(2),
                                Email = reader.GetString(3),
                                Haslo = reader.GetString(4),
                                OstatniaZmianaHasla = reader.GetValue(5).ToString(),
                                Login = reader.GetString(6)
                            });
                        }
                    }
                }
            }
            return retVal;
        }

        // GET api/<controller>/5
        [HttpGet]
        [Authorize]
        [RequireHttps]
        public Kierowca Get(int id)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net"; 
            builder.UserID = "tir-root";            
            builder.Password = "razdwatrzy666!";     
            builder.InitialCatalog = "tir-db";

            Kierowca retVal = null;
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Kierowca]");
                sb.Append("WHERE idKierowca = " + id);
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal = new Kierowca
                            {
                                idKierowca = reader.GetInt32(0),
                                Imie = reader.GetString(1),
                                Nazwisko = reader.GetString(2),
                                Email = reader.GetString(3),
                                Haslo = reader.GetString(4),
                                OstatniaZmianaHasla = reader.GetValue(5).ToString(),
                                Login = reader.GetString(6)
                            };
                        }
                    }
                }
            }
            return retVal;
            //return new Kierowca { idKierowca = id, Imie = "Adam", Nazwisko = "Nieadam", Email = "adam@adam.pl", OstatniaZmianaHasla = "15-05-2017", Haslo = "e8ce87c0124ace199c9d221263a690358c22ceae59bffd4a0647a4a91bbb1347", Login = "adam" };
        }

        // POST api/<controller>
        [HttpPost]
        //[Authorize]
        [RequireHttps]
        public HttpResponseMessage Post(Kierowca kierowca)
        {
            if (kierowca == null || kierowca.Email == null || kierowca.Imie == null || kierowca.Login == null || kierowca.Nazwisko == null || kierowca.OstatniaZmianaHasla == null || kierowca.Haslo == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            else if (!Regex.IsMatch(kierowca.Imie, @"^[a-zA-Z]+$") || !Regex.IsMatch(kierowca.Nazwisko, @"^[a-zA-Z]+$") || !Regex.IsMatch(kierowca.Login, @"^[a-zA-Z0-9]+$"))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            else if (!Regex.IsMatch(kierowca.Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            else if (kierowca.Imie.ToCharArray()[0] > 90 || kierowca.Imie.ToCharArray()[0] > 90)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                
                //Sprawdzenie, czy istnieje już kierowca o podanym Loginie
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT [dbo].[Kierowca].[idKierowca]");
                sb.Append("FROM [dbo].[Kierowca]");
                sb.Append("WHERE Login LIKE '" + kierowca.Login + "'");
                String sql = sb.ToString();
                bool ifExist = false;
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ifExist = true;
                        }
                    }
                }
                if (ifExist)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    //return new HttpResponseMessage(HttpStatusCode.

                //Pobranie największą wartość id
                sb = new StringBuilder();
                sb.Append("SELECT Max([dbo].[Kierowca].[idKierowca])");
                sb.Append("FROM [dbo].[Kierowca]");
                sql = sb.ToString();
                int maxId = 0;
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            maxId = reader.GetInt32(0);
                        }
                    }
                }
                //Nie może pobrać max id
                if (maxId == 0)
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                
                maxId++;
                sql = "INSERT INTO [dbo].[Kierowca] VALUES (@idKierowca, @Imie, @Nazwisko, @Email, @Haslo, @OstatniaZmianaHasla, @Login)";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    DateTime myDate;
                    try
                    {
                        myDate = DateTime.ParseExact(kierowca.OstatniaZmianaHasla, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex) { return new HttpResponseMessage(HttpStatusCode.BadRequest); }

                    command.Parameters.AddWithValue("@idKierowca", maxId.ToString());
                    command.Parameters.AddWithValue("@Imie", kierowca.Imie);
                    command.Parameters.AddWithValue("@Nazwisko", kierowca.Nazwisko);
                    command.Parameters.AddWithValue("@Email", kierowca.Email);
                    command.Parameters.AddWithValue("@Haslo", kierowca.Haslo);
                    command.Parameters.AddWithValue("@OstatniaZmianaHasla", myDate);
                    command.Parameters.AddWithValue("@Login", kierowca.Login);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex) { return new HttpResponseMessage(HttpStatusCode.BadRequest); }

                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }

            //return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT api/<controller>/5
        [HttpPut]
        [Authorize]
        [RequireHttps]
        public void Put(int id, [FromBody]string value)
        {
            //zmiana informacji o kierowcy
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        [Authorize]
        [RequireHttps]
        public HttpResponseMessage Delete(string Login)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                //Sprawdzenie, czy istnieje już kierowca o podanym Loginie
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT [dbo].[Kierowca].[idKierowca]");
                sb.Append("FROM [dbo].[Kierowca]");
                sb.Append("WHERE Login LIKE '" + Login + "'");
                String sql = sb.ToString();
                bool ifExist = false;
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ifExist = true;
                        }
                    }
                }
                if (!ifExist)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                sb = new StringBuilder();
                sb.Append("DELETE ");
                sb.Append("FROM [dbo].[Kierowca]");
                sb.Append("WHERE Login LIKE '" + Login + "'");
                sql = sb.ToString();
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
        }
    }
}