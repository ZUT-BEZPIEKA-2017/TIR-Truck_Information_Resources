﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;
using System.Text;

namespace MvcApplication2.Controllers
{
    public class Parking_ZgloszeniaController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        [Authorize]
        [RequireHttps]
        public IEnumerable<ZgloszeniaParkinigi> Get()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            //Pobranie wszystkich parkingów
            List<Parking> parkings = new List<Parking>();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Parking]");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            parkings.Add(new Parking
                            {
                                idParking = reader.GetInt32(0),
                                Adres = reader.GetString(1),
                                Wspolrzedne_Lat = (double)reader.GetDecimal(2),
                                Wspolrzedne_Lng = (double)reader.GetDecimal(3),
                                Ilosc_Miejsc = reader.GetInt32(4)
                            });
                        }
                    }
                }
            }

            //Pobranie wszyskich rekordów z Parking_Zgloszenia
            List<Parking_Zgloszenia> parkingiZgloszenia = new List<Parking_Zgloszenia>();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Parking_Zgloszenia]");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            parkingiZgloszenia.Add(new Parking_Zgloszenia
                            {
                                idParking_Miejsca = reader.GetInt32(0),
                                Miejsca = reader.GetInt32(1),
                                idKierowca = reader.GetInt32(2),
                                idParking = reader.GetInt32(3),
                                Czas = reader.GetValue(4).ToString()
                            });
                        }
                    }
                }
            }

            //Pobranie wszystkick kierowców
            List<Kierowca> kierowcy = new List<Kierowca>();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Kierowca]");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            kierowcy.Add(new Kierowca
                            {
                                idKierowca = reader.GetInt32(0),
                                Imie = reader.GetString(1),
                                Nazwisko = reader.GetString(2),
                                Email = reader.GetString(3),
                                Haslo = reader.GetString(4),
                                OstatniaZmianaHasla = reader.GetValue(5).ToString(),
                                Login = reader.GetString(6)
                            });
                        }
                    }
                }
            }

            List<ZgloszeniaParkinigi> retVal = new List<ZgloszeniaParkinigi>();
            foreach (Parking park in parkings)
            {
                List<Zgloszenia> zgloszenia = new List<Zgloszenia>();
                foreach (Parking_Zgloszenia pz in parkingiZgloszenia)
                {
                    if (park.idParking == pz.idParking)
                    {
                        //Szukanie loginu kierowcy
                        String login = null;
                        foreach (Kierowca kier in kierowcy)
                        {
                            if (pz.idKierowca == kier.idKierowca)
                            {
                                login = kier.Login;
                                break;
                            }
                        }
                        DateTime date = DateTime.ParseExact(pz.Czas, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime now = DateTime.Now;
                        now.AddHours(-6);
                        //date.AddHours(-6);
                        if (date.CompareTo(now) > 0)
                            zgloszenia.Add(new Zgloszenia
                            {
                                Miejsca = pz.Miejsca,
                                Login = login,
                                Czas = pz.Czas
                            });
                    }
                }
                retVal.Add(new ZgloszeniaParkinigi
                {
                    Parking = park,
                    ListaZgloszen = zgloszenia
                });
            }
            return retVal;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        [Authorize]
        [RequireHttps]
        public HttpResponseMessage Post(Parking_Zgloszenia zgloszenie)
        {
            if (zgloszenie == null || zgloszenie.Czas == null)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            else if (zgloszenie.Miejsca < 0 || zgloszenie.idParking < 0 || zgloszenie.idKierowca < 0)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                //Sprawdzenie liczby miejs
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT [dbo].[Parking].[Miejsca]");
                sb.Append("FROM [dbo].[Parking]");
                sb.Append("WHERE idParking = " + zgloszenie.idParking);
                String sql = sb.ToString();
                int numberOfSpots = -1;
                try
                {
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                numberOfSpots = reader.GetInt32(0);
                            }
                        }
                    }
                }
                catch (SqlException ex) { return new HttpResponseMessage(HttpStatusCode.BadRequest); }
                if (zgloszenie.Miejsca > numberOfSpots)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                //Pobranie największą wartość id
                sb = new StringBuilder();
                sb.Append("SELECT Max([dbo].[Parking_Zgloszenia].[idParking_Miejsca])");
                sb.Append("FROM [dbo].[Parking_Zgloszenia]");
                sql = sb.ToString();
                int maxId = 0;
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            maxId = reader.GetInt32(0);
                        }
                    }
                }
                //Nie może pobrać max id
                if (maxId == 0)
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                
                maxId++;
                sql = "INSERT INTO [dbo].[Parking_Zgloszenia] VALUES (@idParking_Miejsca, @Miejsca, @idKierowca, @idParking, @Czas)";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {

                    //DateTime myDate = DateTime.ParseExact(zgloszenie.Czas, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime myDate;
                    try
                    {
                        myDate = DateTime.ParseExact(zgloszenie.Czas, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        if (DateTime.Now.CompareTo(myDate) > 0)
                            return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }
                    catch (Exception ex) { return new HttpResponseMessage(HttpStatusCode.BadRequest); }

                    command.Parameters.AddWithValue("@idParking_Miejsca", maxId.ToString());
                    command.Parameters.AddWithValue("@Miejsca", zgloszenie.Miejsca);
                    command.Parameters.AddWithValue("@idKierowca", zgloszenie.idKierowca);
                    command.Parameters.AddWithValue("@idParking", zgloszenie.idParking);
                    command.Parameters.AddWithValue("@Czas", myDate);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex) { return new HttpResponseMessage(HttpStatusCode.BadRequest); }

                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}