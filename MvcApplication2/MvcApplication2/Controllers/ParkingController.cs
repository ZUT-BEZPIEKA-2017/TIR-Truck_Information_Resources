﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;
using System.Text;

namespace MvcApplication2.Controllers
{
    public class ParkingController : ApiController
    {
       // GET api/<controller>
        [HttpGet]
        [Authorize]
        [RequireHttps]
        public IEnumerable<Parking> Get()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            List<Parking> retVal = new List<Parking>();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Parking]");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(new Parking
                            {
                                idParking = reader.GetInt32(0),
                                Adres = reader.GetString(1),
                                Wspolrzedne_Lat = (double)reader.GetDecimal(2),
                                Wspolrzedne_Lng = (double)reader.GetDecimal(3),
                                Ilosc_Miejsc = reader.GetInt32(4)
                            });
                        }
                    }
                }
            }
            return retVal;
        }

        // GET api/<controller>/5
        [HttpGet]
        [Authorize]
        [RequireHttps]
        public Parking Get(int id)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "tir-projekt.database.windows.net";
            builder.UserID = "tir-root";
            builder.Password = "razdwatrzy666!";
            builder.InitialCatalog = "tir-db";

            Parking retVal = new Parking();
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT *");
                sb.Append("FROM [dbo].[Parking]");
                sb.Append("WHERE idParking = " + id);
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal = new Parking
                            {
                                idParking = reader.GetInt32(0),
                                Adres = reader.GetString(1),
                                Wspolrzedne_Lat = (double)reader.GetDecimal(2),
                                Wspolrzedne_Lng = (double)reader.GetDecimal(3),
                                Ilosc_Miejsc = reader.GetInt32(4)
                            };
                        }
                    }
                }
            }
            return retVal;
        }

        // POST api/<controller>
        public void Post([FromBody]string id)
        {
            //nowy kieorwca
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
            //zmiana informacji o kierowcy
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
