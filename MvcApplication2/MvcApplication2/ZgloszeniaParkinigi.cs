﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class ZgloszeniaParkinigi
    {
        public Parking Parking { get; set; }
        public List<Zgloszenia> ListaZgloszen { get; set; }
    }
}