﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Konwersacje
    {
        public int idKonwersacje { get; set; }
        public string Tresc { get; set; }
        public string Czas { get; set; }
        public Kierowca nadawca { get; set; }
        public Kierowca odbiorca { get; set; }
    }
}