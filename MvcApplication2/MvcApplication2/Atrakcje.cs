﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Atrakcje
    {
        public int idAtrakcje { get; set; }
        public string Nazwa { get; set; }
        public Rodzaj_Atrakcji rodzaj_atrakcji { get; set; }
        public double Wspolrzedne_Lng { get; set; }
        public double Wspolrzedne_Lat { get; set; }
    }
}