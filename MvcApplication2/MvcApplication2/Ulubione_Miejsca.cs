﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Ulubione_Miejsca
    {
        public int idUlubione_Miejsca { get; set; }
        public Kierowca kierowca { get; set; }
        public Atrakcje atrakcje { get; set; }
    }
}