﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Przeszkody_Zgloszenia
    {
        public int idPrzeszkoda_Zgloszenia { get; set; }
        public Kierowca kieorwca { get; set; }
        public Typ_Przeszkoda typ_przeszkoda { get; set; }
        public string Kiedy { get; set; }
        public bool Aktualnosc { get; set; }
        public double Wspolrzedne_Lng { get; set; }
        public double Wspolrzedne_Lat { get; set; }

    }
}