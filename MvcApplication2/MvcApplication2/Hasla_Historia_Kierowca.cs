﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Hasla_Historia_Kierowca
    {
        public int idHasla_Historia_Kierowca { get; set; }
        public string Haslo { get; set; }
        public string Data { get; set; }
        public Kierowca kierowca { get; set; }
    }
}