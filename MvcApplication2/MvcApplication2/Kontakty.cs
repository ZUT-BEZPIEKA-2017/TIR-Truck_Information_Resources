﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication2
{
    public class Kontakty
    {
        public int idKontakty { get; set; }
        public Kierowca kierowca { get; set; }
        public Kierowca znajomy { get; set; }
    }
}