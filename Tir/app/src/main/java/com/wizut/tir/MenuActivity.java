package com.wizut.tir;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private int lastUsedFragment = R.id.nav_homepage;
    private String login = null;
    private String authorizationData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        final TextView loginTextView = (TextView) header.findViewById(R.id.nav_header_name);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState != null) {
            lastUsedFragment = savedInstanceState.getInt("_lastUsedFragment");
            login = savedInstanceState.getString("_login");
            authorizationData = savedInstanceState.getString("_authorizationData");
            displayView(savedInstanceState.getInt("_lastUsedFragment"));
        } else {
            Bundle bundle = getIntent().getExtras();
            login = bundle.getString("_login");
            authorizationData = bundle.getString("_authorizationData");
            displayView(R.id.nav_homepage);
        }
        loginTextView.setText(login);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("_lastUsedFragment", lastUsedFragment);
        outState.putString("_login", login);
        outState.putString("_authorizationData", authorizationData);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void displayView(int viewId) {

        int title = 0;
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (viewId) {
            case R.id.nav_messenger:
                fragmentClass = MessengerFragment.class;
                title = R.string.messenger;
                break;
            case R.id.nav_map:
                fragmentClass = MapFragment.class;
                title = R.string.map;
                break;
            case R.id.nav_places:
                fragmentClass = ParkingAreasFragment.class;
                title = R.string.places;
                break;
            case R.id.nav_account:
                fragmentClass = AccountFragment.class;
                title = R.string.account;
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                title = R.string.settings;
                break;
            case R.id.nav_about:
                fragmentClass = AboutFragment.class;
                title = R.string.about;
                break;
            case R.id.nav_logout:
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                fragmentClass = HomepageFragment.class;
                title = R.string.homepage;
                break;
        }

        if (fragmentClass != null) {
            getSupportActionBar().setTitle(getString(title));

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                if(viewId == R.id.nav_places){
                    Bundle bundle = new Bundle();
                    bundle.putString("_authorizationData", authorizationData);
                    fragment.setArguments(bundle);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.contentPanel, fragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        lastUsedFragment = item.getItemId();
        displayView(item.getItemId());
        return true;
    }
}
