package com.wizut.tir;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ConnectionTask extends AsyncTask<String, Void, ResponseMessage> {

    public interface ConnectionTaskCompleted {
        void onTaskCompleted(ResponseMessage result);
    }

    public ConnectionTaskCompleted delegate = null;

    public ConnectionTask(ConnectionTaskCompleted delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ResponseMessage doInBackground(String... args) {

        URL url = null;
        BufferedReader reader = null;
        StringBuilder stringBuilder = null;
        HttpsURLConnection urlConnection = null;
        ResponseMessage responseMessage = new ResponseMessage();

        try {
            url = new URL(args[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(10000);

            if (args[1].equals("POST")) {
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
            }

            for (int i = 2; args[i] != null; i += 2) {
                urlConnection.setRequestProperty(args[i], args[i + 1]);
            }

            urlConnection.connect();

            if (args[1].equals("POST")) {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputStreamWriter.write(args[args.length - 1]);
                outputStreamWriter.close();
            }

            responseMessage.code = urlConnection.getResponseCode();
            reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        if (responseMessage.code == HttpURLConnection.HTTP_OK) {
            String line;

            try {
                stringBuilder = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                responseMessage.JSON = stringBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return responseMessage;
    }

    @Override
    protected void onPostExecute(ResponseMessage response) {
        delegate.onTaskCompleted(response);
    }
}
