package com.wizut.tir;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ParkingArea> parkingAreaList;

    public ListViewAdapter(Context context, ArrayList<ParkingArea> parkingAreaList){
        this.context = context;
        this.parkingAreaList = parkingAreaList;
    }

    @Override
    public int getCount() {
        return parkingAreaList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView header = null;
        TextView places = null;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.parking_list_row, null);
        }

        header = (TextView) convertView.findViewById(R.id.parkingItemHeader);
        places = (TextView) convertView.findViewById(R.id.parkingItemPlaces);

        header.setText("Address: " + parkingAreaList.get(position).address);
        places.setText("Parking places: " + parkingAreaList.get(position).amountPlaces);

        return convertView;
    }
}
