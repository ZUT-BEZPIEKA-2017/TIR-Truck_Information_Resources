package com.wizut.tir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText loginInput = (EditText) findViewById(R.id.input_login);
        final EditText passwordInput = (EditText) findViewById(R.id.input_password);
        TextView registerLink = (TextView) findViewById(R.id.link_register);
        final Button loginButton = (Button) findViewById(R.id.buttonLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(loginInput.getText().toString());
                stringBuilder.append(":");
                stringBuilder.append(Security.getSHA256(passwordInput.getText().toString()));
                final String authorizationData = "basic " + Security.getBase64String(stringBuilder.toString());

                ConnectionTask connectionTask = new ConnectionTask(new ConnectionTask.ConnectionTaskCompleted() {
                    @Override
                    public void onTaskCompleted(ResponseMessage result) {
                        boolean status = false;
                        String message;

                        if (result.code == HttpURLConnection.HTTP_OK) {

                            try {
                                JSONObject jsonObject = new JSONObject(result.JSON);

                                status = jsonObject.getBoolean("status");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (status) {
                                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("_login", loginInput.getText().toString());
                                bundle.putString("_authorizationData", authorizationData);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            if (result.code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                                message = "Bad credentials";
                            } else {
                                message = Integer.toString(result.code);
                            }

                            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                });
                connectionTask.execute("https://tirprojekt.azurewebsites.net/api/Log", "GET", "Authorization", authorizationData, null);
            }
        });

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
