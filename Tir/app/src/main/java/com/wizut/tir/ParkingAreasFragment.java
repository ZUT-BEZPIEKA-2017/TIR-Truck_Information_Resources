package com.wizut.tir;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ParkingAreasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ParkingAreasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ParkingAreasFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView listView;
    private ListViewAdapter listViewAdapter;
    private ArrayList<ParkingArea> parkingAreaList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ParkingAreasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ParkingAreasFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ParkingAreasFragment newInstance(String param1, String param2) {
        ParkingAreasFragment fragment = new ParkingAreasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parkingAreaList = new ArrayList<ParkingArea>() {
        };
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_places, container, false);

        ConnectionTask connectionTask = new ConnectionTask(new ConnectionTask.ConnectionTaskCompleted() {
            @Override
            public void onTaskCompleted(ResponseMessage result) {
                Toast toast;
                if (result.code == HttpURLConnection.HTTP_OK) {
                    parseJSON(result);
                    printList(view);
                } else {
                    toast = Toast.makeText(getContext(), Integer.toString(result.code), Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        connectionTask.execute("https://tirprojekt.azurewebsites.net/api/Parking_Zgloszenia", "GET", "Authorization", getArguments().getString("_authorizationData"), null);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void printList(View view){
        listView = (ListView) view.findViewById(R.id.places_list);
        listViewAdapter = new ListViewAdapter(getActivity().getApplicationContext(), parkingAreaList);
        listView.setAdapter(listViewAdapter);
    }

    public void parseJSON(ResponseMessage result) {
        try {
            JSONArray jsonArray = new JSONArray(result.JSON);
            ParkingArea parkingArea;
            UsersReport usersReport;

            for (int i = 0; i < jsonArray.length(); i++) {
                parkingArea = new ParkingArea();
                parkingArea.id = jsonArray.getJSONObject(i).getJSONObject("Parking").getInt("idParking");
                parkingArea.address = jsonArray.getJSONObject(i).getJSONObject("Parking").getString("Adres");
                parkingArea.latitude = jsonArray.getJSONObject(i).getJSONObject("Parking").getDouble("Wspolrzedne_Lat");
                parkingArea.longitude = jsonArray.getJSONObject(i).getJSONObject("Parking").getDouble("Wspolrzedne_Lng");
                parkingArea.amountPlaces = jsonArray.getJSONObject(i).getJSONObject("Parking").getInt("Ilosc_Miejsc");

                for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("ListaZgloszen").length(); j++) {
                    usersReport = new UsersReport();
                    usersReport.freePlaces = jsonArray.getJSONObject(i).getJSONArray("ListaZgloszen").getJSONObject(j).getInt("Miejsca");
                    usersReport.userLogin = jsonArray.getJSONObject(i).getJSONArray("ListaZgloszen").getJSONObject(j).getString("Login");
                    usersReport.time = jsonArray.getJSONObject(i).getJSONArray("ListaZgloszen").getJSONObject(j).getString("Czas");
                    parkingArea.reports.add(j, usersReport);
                }

                parkingAreaList.add(i, parkingArea);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
