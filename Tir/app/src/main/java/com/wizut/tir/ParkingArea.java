package com.wizut.tir;

import java.util.ArrayList;

public class ParkingArea {
    public int id;
    public String address;
    public double latitude;
    public double longitude;
    public int amountPlaces;
    public ArrayList<UsersReport> reports;

    public ParkingArea(){
        reports = new ArrayList<>();
    }
}
