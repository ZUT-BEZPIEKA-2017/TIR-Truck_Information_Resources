package com.wizut.tir;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Security {

    public static String getBase64String(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.DEFAULT).replace("\n", "");
    }

    public static String byteArrayToHex(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length * 2);
        for (byte x : data)
            sb.append(String.format("%02x", x));
        return sb.toString();
    }

    public static String getSHA256(String data) {
        byte[] digestReturn = null;
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(data.getBytes());
            digestReturn = messageDigest.digest();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return byteArrayToHex(digestReturn);
    }
}
