package com.wizut.tir;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class RegisterActivity extends AppCompatActivity {

    boolean nameIsValid = false;
    boolean surnameIsValid = false;
    boolean loginIsValid = false;
    boolean emailIsValid = false;
    boolean passwordIsValid = false;

    EditText nameInput = null;
    EditText surnameInput = null;
    EditText loginInput = null;
    EditText emailInput = null;
    EditText passwordInput = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nameInput = (EditText) findViewById(R.id.input_name);
        surnameInput = (EditText) findViewById(R.id.input_surname);
        loginInput = (EditText) findViewById(R.id.input_login);
        emailInput = (EditText) findViewById(R.id.input_email_address);
        passwordInput = (EditText) findViewById(R.id.input_password);

        TextView loginLink = (TextView) findViewById(R.id.link_login);
        Button registerButton = (Button) findViewById(R.id.btnRegister);

        nameInput.addTextChangedListener(new TextValidator(nameInput) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Name is empty");
                    nameIsValid = false;
                } else if (text.length() > 16){
                    textView.setError("Name is too long");
                    nameIsValid = false;
                } else if (!text.matches("^[a-zA-Z]*$")) {
                    textView.setError("Name is not allowed");
                    nameIsValid = false;
                } else {
                    nameIsValid = true;
                }
            }
        });

        surnameInput.addTextChangedListener(new TextValidator(surnameInput) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Surname is empty");
                    surnameIsValid = false;
                } else if (text.length() > 16){
                    textView.setError("Surname is too long");
                    nameIsValid = false;
                }  else if (!text.matches("^[a-zA-Z]*$")) {
                    textView.setError("Surname is not allowed");
                    surnameIsValid = false;
                } else {
                    surnameIsValid = true;
                }
            }
        });

        loginInput.addTextChangedListener(new TextValidator(loginInput) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Login is empty");
                    loginIsValid = false;
                } else if (text.length() > 24){
                    textView.setError("Login is too long");
                    nameIsValid = false;
                }  else if (!text.matches("^[a-zA-Z0-9]*$")) {
                    textView.setError("Login is not allowed");
                    loginIsValid = false;
                } else {
                    loginIsValid = true;
                }
            }
        });

        emailInput.addTextChangedListener(new TextValidator(emailInput) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Email is empty");
                    emailIsValid = false;
                } else if (text.length() > 48){
                    textView.setError("Email is too long");
                    nameIsValid = false;
                }  else if (!text.matches("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")) {
                    textView.setError("Email is not allowed");
                    emailIsValid = false;
                } else {
                    emailIsValid = true;
                }
            }
        });

        passwordInput.addTextChangedListener(new TextValidator(passwordInput) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() < 8) {
                    textView.setError("Password is too short");
                    passwordIsValid = false;
                } else if (text.length() > 32){
                    textView.setError("Password is too long");
                    nameIsValid = false;
                }  else {
                    passwordIsValid = true;
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameIsValid && surnameIsValid && loginIsValid && emailIsValid && passwordIsValid) {

                    ConnectionTask connectionTask = new ConnectionTask(new ConnectionTask.ConnectionTaskCompleted() {

                        @Override
                        public void onTaskCompleted(ResponseMessage result) {
                            boolean status = false;
                            String message;

                            if (result.code == HttpURLConnection.HTTP_OK) {
                                    finish();
                            } else {
                                if (result.code == HttpURLConnection.HTTP_BAD_REQUEST) {
                                    message = "User exist in database";
                                } else {
                                    message = Integer.toString(result.code);
                                }

                                Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    });
                    connectionTask.execute("https://tirprojekt.azurewebsites.net/api/Kierowca", "POST", "Content-Type", "application/json", null, convertDataToJSON());
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct your form", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private String convertDataToJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("idKierowca", 0);
            jsonObject.put("Imie", nameInput.getText().toString());
            jsonObject.put("Nazwisko", surnameInput.getText().toString());
            jsonObject.put("Email", emailInput.getText().toString());
            jsonObject.put("Haslo", Security.getSHA256(passwordInput.getText().toString()));
            jsonObject.put("OstatniaZmianaHasla", "13.06.2017");
            jsonObject.put("Login", loginInput.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
